#include <vector>
#include <numeric>
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <sstream>
using namespace std;

class Row {
public:
	vector<int> nums;

	Row() {}

	// constructor using a vector of integers
	Row(vector<int> tempNums) {
		nums.assign(tempNums.begin(), tempNums.end());
	}

	// copy constructor
	Row(Row& row2) {
		for (auto i : row2.nums)
			nums.push_back(i);
	}

	// add an element to the end of the row
	void addElement(int x) {
		nums.push_back(x);
	}

	// return the next row of the current row
	Row nextRow() {
		Row newRow({ 1 });
		if (nums.size() == 0)
			return newRow;
		else {
			for (int i = 1; i < (int)nums.size(); i++)
				newRow.addElement(nums[i - 1] + nums[i]);
			newRow.addElement(1);
			return newRow;
		}
	}

	// return the max size of the integer in the row
	int maxIntegerSize() {
		int res = 0;
		for (auto elt : nums)
			res = max(res, (int)to_string(elt).size());
		return res;
	}

	// print the row according to the given brick size
	void printRow(int brickSize) {
		for (auto elt : nums) {
			int eltSize = to_string(elt).size();
			int preSize = (brickSize - eltSize) / 2;
			cout << string(preSize, ' ') << elt << string(brickSize - preSize - eltSize, ' ') << string(brickSize, ' ');
		}
	}
};


class Triangle {
public:
	vector<Row> rows;
	Triangle() {}

	// construct the triangle if given its size
	Triangle(int x) {
		if (x > 0) {
			rows.push_back(Row({ 1 }));
			for (int i = 1; i < x; i++)
				rows.push_back(rows.back().nextRow());
		}
	}

	// print the triangle
	void printTriangle() {
		int brickSize = 0;
		for (auto elt : rows)
			brickSize = max(brickSize, elt.maxIntegerSize());

		for (auto elt : rows) {
			for (int i = 0; i < (int)rows.size() - (int)elt.nums.size(); i++)
				cout << string(brickSize, ' ');
			elt.printRow(brickSize);
			cout << endl;
		}
	}

};


int
main(int argc, char** argv)
{
	int x = 8;
	if(argc > 1)
		x = atoi(argv[1]);

	Triangle triangle(x);
	triangle.printTriangle();

	return 0;
}
