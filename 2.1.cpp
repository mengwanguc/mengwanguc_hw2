#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

// print a vector of floats
void printFloats(vector<double> &floats) {
	for(auto it = floats.begin(); it != floats.end(); it++){
		cout << *it << "  " ;
	}
	cout << endl;
}

// calculate the square of a float
double square(double f) {
	return f * f;
}

//  produce a new vector with each element equal to the square of the corresponding element in the original vector 
vector<double> produceFloatsSquare(vector<double> &origin) {
	vector<double> outputs;
	outputs.resize(origin.size());
	transform(origin.begin(), origin.end(), outputs.begin(), square);
	return outputs;
}

// return x + y * y. This function is used in part 3 for accumulate
double squarePlus(double x, double y) {
	return x + y * y;
}


int main(int argc, char** argv) {
	vector<double> inputs;
	int n;
	cout << "How many floats do you want to input? ";
	cin >> n;
	cout << "Please input your floats: ";
	for (int i = 0; i < n; i++) {
		double temp;
		cin >> temp;
		inputs.push_back(temp);
	}

	// part 1
	cout << "------------------------------------------------------\nPart 1 \n";
	vector<double> outputs;
	outputs = produceFloatsSquare(inputs);
	cout << "The new vector constains: ";
	printFloats(outputs);

	// part 2
	cout << "------------------------------------------------------\nPart 2 \n";
	double sum = 0;
	double squareRoot;
	sum = accumulate(outputs.begin(), outputs.end(), sum);
	squareRoot = sqrt(sum);
	cout << "The sum of elements in the new vector is:  "<<sum<<endl;
	cout << "The square root is:  " << squareRoot << endl;

	// part 3
	cout << "------------------------------------------------------\nPart 3 \n";
	double output3 = 0;
	double squareRoot3;
	output3 = accumulate(inputs.begin(), inputs.end(), output3, squarePlus);
	cout << "The sum of elements in the new vector is:  " << output3 << endl;
	squareRoot3 = sqrt(output3);
	cout << "The square root is:  " << squareRoot3 << endl;
}