#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

// median part 1
double median1(vector<double> inputs) {
	int n = inputs.size();
	sort(inputs.begin(), inputs.end());
	
	if (n % 2 == 1)
		return inputs[n / 2];
	else
		return (inputs[n / 2 - 1] + inputs[n / 2]) / 2;
}

// median part 2
double median2(vector<double> inputs) {
	int n = inputs.size();
	partial_sort(inputs.begin(), inputs.begin() + n / 2 + 1, inputs.end());

	if (n % 2 == 1)
		return inputs[n / 2];
	else
		return (inputs[n / 2 - 1] + inputs[n / 2]) / 2;
}

// median part 3
double median3(vector<double> inputs) {
	int n = inputs.size();
	if (n % 2 == 1) {
		nth_element(inputs.begin(), inputs.begin() + n / 2 + 1, inputs.end());
		return inputs[n / 2];
	}
	else {
		nth_element(inputs.begin(), inputs.begin() + n / 2, inputs.end());
		nth_element(inputs.begin() + n / 2, inputs.begin() + n / 2 + 1, inputs.end());
		return (inputs[n / 2 - 1] + inputs[n / 2]) / 2;
	}
}

// median part 4
template<typename T>
T median4(vector<T> inputs) {
	int n = inputs.size();
	sort(inputs.begin(), inputs.end());
	if (n % 2 == 1)
		return inputs[n / 2];
	else
		return (inputs[n / 2 - 1] + inputs[n / 2]) / 2;
}

int main(int argc, char** argv) {
	vector<double> inputs;
	int n;
	cout << "How many floats do you want to input? ";
	cin >> n;
	cout << "Please input your floats: ";
	for (int i = 0; i < n; i++) {
		double temp;
		cin >> temp;
		inputs.push_back(temp);
	}

	// part 1
	cout << "--------------------------------------------\nPart 1\n";
	double res1;
	res1 = median1(inputs);
	cout << "The median is:  " << res1 << endl;

	// part 2
	cout << "--------------------------------------------\nPart 2\n";
	double res2;
	res2 = median2(inputs);
	cout << "The median is:  " << res2 << endl;

	// part 3
	cout << "--------------------------------------------\nPart 3\n";
	double res3;
	res3 = median3(inputs);
	cout << "The median is:  " << res3 << endl;

	// part 4
	cout << "--------------------------------------------\nPart 4\n";
	double res4;
	res4 = median4(inputs);
	cout << "The median is:  " << res4 << endl;

}