#include <iostream>
#include <string>
#include <vector>

using namespace std;

class TreeNode {
public:
	string name;
	vector<TreeNode*> children;
	// constructor
	TreeNode(string name) {
		this->name = name;
	}

	// find child
	TreeNode* findChild(string name) {
		for (auto child : children)
			if (child->name == name)
				return child;
		return NULL;
	}

	// add child
	TreeNode* addChild(string childName) {
		TreeNode* newChild = new TreeNode(childName);
		children.push_back(newChild);
		return newChild;
	}

	// print tree node
	void printTreeNode(int n) {
		cout << string(n, '\t') << name << endl;
		for (auto child : children)
			child->printTreeNode(n + 1);
	}
};


class Tree {
public:
	TreeNode* root;
	
	Tree(string rootName) {
		root = new TreeNode(rootName);
	}

	// insert a path into a tree
	void insert(string path) {
		size_t l = 0, r;
		TreeNode* temp = root;
		while ((r = path.find_first_of('/', l)) < path.size()) {
			string name = path.substr(l, r - l);
			TreeNode* child;
			if (((child = temp->findChild(name)) != NULL))
				temp = child;
			else
				temp = temp->addChild(name);
			l = r + 1;
		}
	}

	// print the whole tree
	void printTree() {
		for (auto child : root->children)
			child->printTreeNode(0);
	}
};


int main(int argc, char** argv) {
	if (argc <= 1)
		return 0;
	
	// construct a tree
	Tree* tree = new Tree("");

	// insert all the paths into the tree
	for (int i = 1; i < argc; i++) {
		string path(argv[i]);
		if (path.back() != '/')
			path.push_back('/');
		tree->insert(path);
	}

	// print the tree
	tree->printTree();
	
	return 0;
}