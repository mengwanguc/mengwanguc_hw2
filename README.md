### What is this repository for? ###

This is my solutions to homework 2 in the course C++.


### hw 2.1 ###

2.1.cpp is the source code of my solution to hw 2.1. All the three parts are contained in this code. When running the program, the results of these three parts are shown separately.


### hw 2.2 ###

2.2.cpp is the source code of my solution to hw 2.2. All the four parts are contained in this code. When running the program, the results of these four parts are shown separately.

In part 1, the time complexity of the algorithm is O(n * log n)

In part 2, the algorithm is more efficient because we don't need to sort the whole vector. However, it should noticed that the time complexity is still O(n * log n)

In part 3, the algorithm is more efficient because it takes O(n) time to find the nth element. The time complexity of the algorithm is O(n).



### hw 2.3 ###

2.3.cpp is the source code of my solution to hw 2.3.


### hw 2.4 ###

2.4.cpp is the source code of my solution to hw 2.4. I constructed a tree (similar to a prefix tree) to solve the problem.


### hw 2.5 ###

2.5.cpp is the source code of my solution to hw 2.5.